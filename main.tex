\documentclass{marticl}

\usepackage{asymptote}
	\begin{asydef}
		usepackage("newpxtext, newpxmath");
	\end{asydef}

\usepackage{geometry}
	\geometry{margin = 3cm}

\usepackage{float}

\usepackage{amsmath}

\DeclareMathOperator{\range}{range}

\title{The Cantor Schroeder-Bernstein Theorem}
\author{Dennis Chen}

\begin{document}

\maketitle

\begin{abstract}
	The Cantor Schroeder-Bernstein Theorem states that if there exists an injection $f\colon A\to B$ and an injection $g\colon B\to A$, then there exists a bijection $h\colon A \to B$. This is an intuitive result, but its proof is surprisingly tricky.

	In this article, we briefly review injections, surjections, and bijections, and use these basic facts to support a proof of the Cantor Schroeder-Bernstein Theorem.
\end{abstract}

Much of this article was derived from \url{https://web.williams.edu/Mathematics/lg5/CanBer.pdf}. Here we just fill in all of the details.

\section{Preliminaries}

This should mostly be review. You should work out proofs of each of these theorems yourself.

\subsection{Functions}

\begin{defi}[Injection]
	A function $f \colon A \to B$ is \emph{injective} if and only if $f(x) = f(y)$ implies $x = y$.
\end{defi}

In other words, if the outputs are the same, the inputs must be the same.

\begin{defi}[Surjection]
	A function $f \colon A \to B$ is \emph{surjective} if for all $b \in B$, there exists some $a \in A$ such that $f(a) = b$.
\end{defi}

\begin{defi}[Bijection]
	A function $f \colon A \to B$ is a bijection if it is an injection and it is a surjection.
\end{defi}

Prove the following theorem.

\begin{theo}[Bijectivity is an equivalence relation]
	We say that $A \sim B$ if and only if there exists a bijection $f \colon A \to B$. Then $\sim$ is an \emph{equivalence relation}. That is,
	\begin{enumerate}
		\item Reflexivity: $A \sim A$.
		\item Symmetry: $A \sim B$ implies $B \sim A$.
		\item Transitivity: $A \sim B$ and $B \sim C$ implies $A \sim C$.
	\end{enumerate}
\end{theo}

\subsection{Unions and Intersections}

\begin{defi}[Union]
	Consider a (possibly infinite) series of sets $A_i$. Then $\bigcup A_i$ is the set of elements contained in at least one $A_i$.
\end{defi}

\begin{defi}[Intersection]
	Consider a (possibly infinite) series of sets $A_i$. Then $\bigcap A_i$ is the set of elements contained in every $A_i$.
\end{defi}

From here on out we will commit an abuse of notation. Given a function $f\colon A \to B$ and $A_0 \subseteq A$, we write $f(A_0)$ to denote the \emph{range} of $f$ on $A_0$. In other words, $f(A_0)$ (which is a subset of $B$) is the set of elements $b$ such that there exists some element $a \in A_0$ where $f(a) = b$.

The following theorem will be useful for the last part of our proof.

\begin{theo}
	\label{theo:f-comp}
	Given a set $A$, a series of sets $A_i\subseteq A$, and an injective function $f \colon \bigcup A_i \to B$,
	\begin{align*}
		f(\bigcup A_i) &= \bigcup f(A_i) \\
		f(\bigcap A_i) &= \bigcap f(A_i)
	\end{align*}
\end{theo}

\section{Cantor Schroeder-Bernstein}

\subsection{Groundwork}

Define sequences $A_n$ and $B_n$ recursively as follows:

\begin{enumerate}
	\item $A_0 = A$
	\item $B_0 = B$
	\item $A_n = g(B_{n-1})$ for $n \geq 1$
	\item $B_n = f(A_{n-1})$ for $n \geq 1$
\end{enumerate}

The following two lemmas are the meat of the proof.

\begin{lemma}
	\label{lemma:sim}
	For $n \geq 0$, $A_n \sim B_{n+1}$.
\end{lemma}

\begin{pro}
	Recall that $B_{n+1} = g(A_n)$. Note that $g\colon A_n \to B_{n+1}$ is injective as $g$ is injective on the entirety of $A$. And since $B_{n+1} = \range A_n$ by definition, $g$ is also surjective. Thus $g\colon A_n \to B_{n+1}$ is a bijection.
\end{pro}

By symmetry, we have $B_n \sim A_{n+1}$ as well.

\begin{lemma}
	\label{lemma:inclusion}
	For $n \geq 0$, $A_n \supseteq A_{n+1}$ and $B_{n+1} \supseteq B_n$.
\end{lemma}

Let us first look at a few small cases to gain some intuition. Note that

\[(A_0, A_1, A_2, A_3) = (A, gB, gfA, gfgB).\]

Notice for every term except for $A_0$, there is a $g$ on the ``outside''. So if we can show inclusion on the sets $g$ is being applied on, then we can also show inclusion on the result after $g$ is implied.

As a concrete example,

\[B \supseteq fA \implies gB \supseteq gfA.\]

\begin{pro}
	We induct on $n$. The base case is straightforward, so we omit it.

	Recall that $A_n = g(B_{n-1})$ and $A_{n+1} = g(B_n)$. Since $B_{n-1} \supseteq B_n$, we conclude that $g(B_{n-1}) \supseteq g(B_n)$. By symmetry, we have $B_n \supseteq B_{n+1}$ as well.
\end{pro}

\subsection{Using our lemmas}

Now there are two cases. Either there exists some $n$ such that $A_n = A_{n+1}$ or $B_n = B_{n+1}$, in which case we are done, or there does not. The full details of the first case are left to Appendix \ref{section:non-strict}, but here is a general sketch. We have $A_n \sim A_{n+1} \sim B_n$, where the important part is $A_n \sim B_n$. We can show that $A_n \sim B_n \implies A_{n-1} \sim B_{n-1}$, which eventually cascades to $A_0 \sim B_0$.

Now suppose that there exists no $n$ such that $A_n = A_{n+1}$ or $B_n = B_{n+1}$. Then we can rewrite Lemmas \ref{lemma:inclusion} and \ref{lemma:sim} as follows.

\begin{lemma}
	\label{lemma:strict-inclusion}
	For $n\geq 0$, $A_n \supsetneq A_{n+1}$ and $B_n \supsetneq B_{n+1}$.
\end{lemma}

Then define $A^{\star}_n$ as $A_n - A_{n+1}$ (where $-$ is set subtraction). Define $B^{\star}$ similarly. Note that $A^{\star}_n$ is never empty as the inclusions in Lemma \ref{lemma:strict-inclusion} are strict and $A_n$ is never empty. This is a simple proof by induction; full details in Appendix \ref{section:non-empty}.

\begin{lemma}
	For $n \geq 0$, $A^{\star}_n \sim B^{\star}_{n+1}$.
\end{lemma}

The proof is left to Appendix \ref{section:star-bijection}.

\begin{lemma}
	There exists a bijection $h_0 \colon \bigcup A_i \to \bigcup B_i$.
\end{lemma}

This is a consequence of Lemma \ref{lemma:dub}.

Let's take stock of where we are. We have bijected most of $A$ to most of $B$, and with Lemma \ref{lemma:dub} we have a tool to compose bijections of disjoint unions. So all we have to do is answer the following questions:

\begin{enumerate}
	\item What part of $A$ is not in $\bigcup A_i^{\star}?$
	\item How do we biject it to its counterpart in $B$?
\end{enumerate}

\begin{lemma}
	The disjoint union of $\bigcup A_i^{\star}$ and $\bigcap A_i$ is $A$.
\end{lemma}

\begin{pro}
	Note that $a \in A$ is in $\bigcup A_i^{\star}$ if and only if there exists some $n$ such that $a \in A_n$ but $a \not\in A_{n+1}$. If there exists no such $n$, then because $a \in A_0$, we conclude $a$ is in every $A_i$. In other words, $a \in \bigcap A_i$.
\end{pro}

\begin{lemma}
	The function $f$ is a bijection from $\bigcap A_i$ to $\bigcap B_i$.
\end{lemma}

\begin{pro}
	Note by Theorem \ref{theo:f-comp} that
	\[f(\bigcap A_i) = \bigcap B_{i+1},\]
	and $B_0 \bigcap B_{i+1} = \bigcap B_i$ as every $B_i$ is a subset of $B_0$.

	So the range of $f(\bigcap A_i)$ is exactly $\bigcap B_i$, meaning that $f\colon \bigcap A_i \to \bigcap B_i$ is a surjection. Since $f$ is injective by definition, $f$ is a bijection, as desired.
\end{pro}

To finish, note that by Lemma \ref{lemma:dub}, $\bigcup A_i^{\star} \sim \bigcup B_i^{\star}$ and $\bigcap A_i \sim \bigcap B_i$ implies $A \sim B$.

\appendix

\section{Proof of non-strict inclusion case}

\label{section:non-strict}

Here we handle the full details of the non-strict inclusion case as a separate theorem.

\begin{theo}
	For any $n\geq 0$, $A_n = A_{n+1} \implies A_0 \sim B_0$.
\end{theo}

If we show this, we show by symmetry that $B_n = B_{n+1} \implies A_0 \sim B_0$.

\begin{lemma}
	\label{lemma:ns-induct}
	For any $n \geq 0$, $A_n \sim B_n \implies A_0 \sim B_0$.
\end{lemma}

\begin{pro}
	We proceed by induction on $n$. The base case of $n = 0$ is obvious.

	Now suppose $A_n \sim B_n$; we want to show that $A_{n+1} \sim B_{n+1}$. But by Lemma \ref{lemma:sim},
	\[B_n \sim A_{n+1} \sim B_{n+1} \sim A_n,\]
	which implies that $A_0 \sim B_0$.
\end{pro}

Note that $A_n \sim A_{n+1} \sim B_n$ by Lemma \ref{lemma:sim}, which implies $A_0 \sim B_0$ by Lemma \ref{lemma:ns-induct}, as desired.

\section{$A_n$ is non-empty}

\label{section:non-empty}

\begin{lemma}
	For all $n \geq 0$, $A_n$ and $B_n$ are non-empty.
\end{lemma}

\begin{pro}
	We induct on $n$. This is obviously true for $n = 0$.\footnote{This is not strictly true, but the case where $A_0 = B_0 = \emptyset$ is so trivial that we don't care.}

	Now suppose $A_n$ and $B_n$ are non-empty; we want to show that $A_{n+1}$ and $B_{n+1}$ are non-empty. But $A_{n+1}$ is the range of $g(B_n)$, and since $B_n$ is non-empty, all we must do to show $A_{n+1}$ is non-empty is select an element in $B_n$.

	Symmetrically, $B_{n+1}$ is non-empty.
\end{pro}

\section{Proof of bijection between $A^{\star}_n$ and $B^{\star}_{n+1}$}

\label{section:star-bijection}

\begin{lemma}
	For all $n\geq 0 $, there is a bijection between $A_n - A_{n+1}$ and $B_{n+1} - B_{n+2}$.
\end{lemma}

\begin{pro}
	Note that $f$ bijects $A_n$ to $B_{n+1}$, and furthermore, it also bijects $A_{n+1}$ into $B_{n+2}$. Since $A_n \supseteq A_{n+1}$ and $B_n \supseteq B_{n+1}$,
	\[f(A_n - A_{n+1}) = f(A_n) - f(A_{n+1}) = B_{n+1} - B_{n+2}.\]
	Since $f$ is injective, $f$ bijects $A_n - A_{n+1}$ to $B_{n+1} - B_{n+2}$.
\end{pro}

\section{Disjoint union bijections}

This is a lemma that is generally useful even outside of this specific proof.

\begin{lemma}
	\label{lemma:dub}
	Suppose there exists a sequence of pairwise disjoint sets $A_i$ and another such sequence $B_i$. Then,
	\[\bigcup A_i \sim \bigcup B_i.\]
\end{lemma}

\begin{pro}
	Select a bijection $f_i \colon A_i \to B_i$ for each $i$. Then, we can explicitly construct a bijection $f \colon \bigcup A_i \to B_i$ as follows: if $a \in A_i$, then $f(a) = f_i(a)$. This is well defined because $a \in A_i$ for exactly one $i$.
	\begin{figure}[H]
		\centering
		\begin{asy}
			size(6cm);
			pair trans = (3, 0);
			pair width = (1, 0);

			pair A0 = (0, 0);
			pair A1 = (0, 1);
			pair A2 = (0, 2);
			pair A3 = (0, 3);
			pair B0 = A0 + width;
			pair B1 = A1 + width;
			pair B2 = A2 + width;
			pair B3 = A3 + width;
			draw(A0--B0, blue);
			draw(A1--B1, blue);
			draw(A2--B2, blue);
			draw(A3--B3, blue);
			draw(A0--A3, blue);
			draw(B0--B3, blue);
			label("$\bigcup A_i$", (A3 + B3)/2, N);
			label("$A_0$", (A3 + B3 + A2 + B2)/4);
			label("$A_1$", (A2 + B2 + A1 + B1)/4);
			label("$\dots$", (A1 + B1 + A0 + B0)/4);

			label("$f_0$", (A3 + A2)/2 - (0.35, 0));
			label("$f_1$", (A2 + A1)/2 - (0.35, 0));
			label("$\dots$", (A1 + A0)/2 - (0.35, 0));
			draw("$f$", brace(A3 - (0.8, 0), A0 - (0.8, 0), -0.25), 2 * W);

			draw(A0 + trans--B0 + trans, red);
			draw(A1 + trans--B1 + trans, red);
			draw(A2 + trans--B2 + trans, red);
			draw(A3 + trans--B3 + trans, red);
			draw(A0 + trans--A3 + trans, red);
			draw(B0 + trans--B3 + trans, red);
			label("$\bigcup B_i$", (A3 + B3)/2 + trans, N);
			label("$B_0$", (A3 + B3 + A2 + B2)/4 + trans);
			label("$B_1$", (A2 + B2 + A1 + B1)/4 + trans);
			label("$\dots$", (A1 + B1 + A0 + B0)/4 + trans);

			draw((B3 + B2)/2 + (0.5, 0) -- (A3 + A2)/2 + trans - (0.4, 0), arrow = Arrow(4));
			draw((B2 + B1)/2 + (0.5, 0) -- (A2 + A1)/2 + trans - (0.4, 0), arrow = Arrow(4));
			draw((B1 + B0)/2 + (0.5, 0) -- (A1 + A0)/2 + trans - (0.4, 0), arrow = Arrow(4));
		\end{asy}
	\end{figure}
\end{pro}

\end{document}
